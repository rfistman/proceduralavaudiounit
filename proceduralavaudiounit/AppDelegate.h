//
//  AppDelegate.h
//  proceduralavaudiounit
//
//  Created by Rhythmic Fistman on 15/03/2017.
//  Copyright © 2017 Rhythmic Fistman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

