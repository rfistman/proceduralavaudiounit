//
//  main.m
//  proceduralavaudiounit
//
//  Created by Rhythmic Fistman on 15/03/2017.
//  Copyright © 2017 Rhythmic Fistman. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
