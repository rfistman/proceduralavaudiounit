//
//  AppDelegate.m
//  proceduralavaudiounit
//
//  Created by Rhythmic Fistman on 15/03/2017.
//  Copyright © 2017 Rhythmic Fistman. All rights reserved.
//

#import "AppDelegate.h"

// http://stackoverflow.com/questions/42798284/custom-avaudiounit-with-avaudioengine-crashes-on-set-provider-block
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// TEST
	AudioComponentDescription mixerDesc;
	mixerDesc.componentType = kAudioUnitType_Generator;
	mixerDesc.componentSubType = kAudioUnitSubType_ScheduledSoundPlayer;
	mixerDesc.componentManufacturer = kAudioUnitManufacturer_Apple;
	mixerDesc.componentFlags = 0;
	mixerDesc.componentFlagsMask = 0;
	
	[AVAudioUnit instantiateWithComponentDescription:mixerDesc options:kAudioComponentInstantiation_LoadInProcess completionHandler:^(__kindof AVAudioUnit * _Nullable audioUnit, NSError * _Nullable error) {
		NSLog(@"here");
		
		// Crashes here
		audioUnit.AUAudioUnit.outputProvider = ^AUAudioUnitStatus(AudioUnitRenderActionFlags *actionFlags, const AudioTimeStamp *timestamp, AUAudioFrameCount frameCount, NSInteger inputBusNumber, AudioBufferList *inputData)
		{
			const double amplitude = 0.2;
			static double theta = 0.0;
			double theta_increment = 2.0 * M_PI * 880.0 / 44100.0;
			const int channel = 0;
			Float32 *buffer = (Float32 *)inputData->mBuffers[channel].mData;
			
			memset(inputData->mBuffers[channel].mData, 0, inputData->mBuffers[channel].mDataByteSize);
			memset(inputData->mBuffers[1].mData, 0, inputData->mBuffers[1].mDataByteSize);
			
			// Generate the samples
			for (UInt32 frame = 0; frame < inputBusNumber; frame++)
			{
				buffer[frame] = sin(theta) * amplitude;
				
				theta += theta_increment;
				if (theta >= 2.0 * M_PI)
				{
					theta -= 2.0 * M_PI;
				}
			}
			
			return noErr;
		};
		
	}];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}


@end
